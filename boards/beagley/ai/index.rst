.. _beagley-ai-home:

BeagleY-AI
##########

.. important::
    This is a work in progress, for latest documentation please 
    visit https://docs.beagleboard.org/latest/

BeagleY-AI is an open-source single board computer based on the Texas Instruments AM67A Arm-based vision processor.

.. admonition:: License Terms

   * This documentation is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__
   * Design materials and license can be found in the `git repository <https://openbeagle.org/beagley-ai/>`__
   * Use of the boards or design materials constitutes an agreement to the :ref:`boards-terms-and-conditions`
   * Software images and purchase links are available on the `board page <https://www.beagleboard.org/boards/beagley-ai>`__
   * For export, emissions and other compliance, see :ref:`beagley-ai-support`
   * All support for BeagleY-AI design is through the BeagleBoard.org community at `BeagleBoard.org forum <https://forum.beagleboard.org/tag/beagley-ai>`_.

.. image:: images/beagley-ai-board.png

.. only:: html

    .. grid:: 1 1 2 2
        :margin: 4 4 0 0
        :gutter: 4
       
        .. grid-item-card::
            :link: beagley-ai-introduction
            :link-type: ref

            **1. Introduction**
            ^^^

            .. image:: images/chapter-thumbnails/01-introduction.*
                :align: center
                :alt: BeagleY-AI Chapter01 thumbnail

        .. grid-item-card:: 
            :link: beagley-ai-quick-start
            :link-type: ref

            **2. Quick start**
            ^^^

            .. image:: images/chapter-thumbnails/02-quick-start.*
                :align: center
                :alt: BeagleY-AI Chapter02 thumbnail

        .. grid-item-card:: 
            :link: beagley-ai-design
            :link-type: ref

            **3. Design & Specifications**
            ^^^

            .. image:: images/chapter-thumbnails/03-design-and-specifications.*
                :align: center
                :alt: BeagleY-AI Chapter03 thumbnail

        .. grid-item-card:: 
            :link: beagley-ai-expansion
            :link-type: ref

            **4. Expansion**
            ^^^

            .. image:: images/chapter-thumbnails/04-connectors-and-pinouts.*
                :align: center
                :alt: BeagleY-AI Chapter04 thumbnail
            
        .. grid-item-card:: 
            :link: beagley-ai-demos
            :link-type: ref

            **5. Demos**
            ^^^

            .. image:: images/chapter-thumbnails/05-demos-and-tutorials.*
                :align: center
                :alt: BeagleY-AI Chapter5 thumbnail

        .. grid-item-card:: 
            :link: beagley-ai-support
            :link-type: ref

            **6. Support**
            ^^^

            .. image:: images/chapter-thumbnails/06-support-documents.*
                :align: center
                :alt: BeagleY-AI Chapter6 thumbnail

.. toctree::
    :maxdepth: 1
    :hidden:

    01-introduction
    02-quick-start
    03-design
    04-expansion
    05-demos
    06-support
